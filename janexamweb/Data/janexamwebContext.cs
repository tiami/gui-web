﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using janexamweb.Data;

namespace janexamweb.Models
{
    public class janexamwebContext : DbContext
    {
        public janexamwebContext (DbContextOptions<janexamwebContext> options)
            : base(options)
        {
        }

        public DbSet<janexamweb.Data.Expense> Expense { get; set; }
    }
}
