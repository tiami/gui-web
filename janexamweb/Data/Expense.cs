﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace janexamweb.Data
{
    public class Expense
    {
        
        public int ExpenseId { get; set; }
        public Job ExpenseJob { get; set; }

        public DateTime ExpenseDate { get; set; }
        public int ExpenseAmount { get; set; }
        public string ExpenseComment { get; set; }
    }
}
