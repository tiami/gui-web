﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace janexamweb.Data
{
    [Serializable]
    public class Job
    {

        public int JobId { get; set; }
        public string JobCustomer { get; set; }
        public DateTime JobDate { get; set; }
        public int JobDuration { get; set; }
        public string JobAddress { get; set; }
        public int JobModels { get; set; }
        public string JobComments { get; set; }


        public override string ToString()
        {
            return JobId + ": " + JobModels + " models at " + JobDate.ToString("d") + ", for " + JobDuration + " days \n"
                   + "Customer: " + JobCustomer + ", Address: " + JobAddress + "\n"
                   + "Comments: " + JobComments;
        }

    }
}
