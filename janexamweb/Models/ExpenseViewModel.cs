﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using janexamweb.Data;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace janexamweb.Models
{
    public class ExpenseViewModel
    {
        public int ExpenseId { get; set; }
        public Job ExpenseJob { get; set; }

        [DataType(DataType.Date)] public DateTime ExpenseDate { get; set; }
        public int ExpenseAmount { get; set; }
        public string ExpenseComment { get; set; }

        public List<String> JobList { set; get; }

        public ExpenseViewModel()
        {
            JobList = new List<String>();
            //string ConString =
            //    "Server=(localdb)\\mssqllocaldb;Database=aspnet-janexamweb-07C271F6-CCC2-4FC4-A05E-5BAFF8EDDF56;Trusted_Connection=True;MultipleActiveResultSets=true";
            //string CmdString = string.Empty;
            //using (SqlConnection con = new SqlConnection(ConString))
            //{
            //    CmdString = "SELECT * FROM dbo.Jobs";
            //    SqlCommand cmd = new SqlCommand(CmdString, con);
            //    SqlDataAdapter sda = new SqlDataAdapter(cmd);
            //    DataTable dt = new DataTable("Jobs");
            //    sda.Fill(dt);
            //    var jobList = (from DataRow dr in dt.Rows
            //        select new Job()
            //        {
            //            JobId = Convert.ToInt32(dr["JobId"]),
            //            JobCustomer = dr["JobCustomer"].ToString(),
            //            JobAddress = dr["JobAddress"].ToString(),
            //            JobDuration = Convert.ToInt32(dr["JobDuration"]),
            //            JobComments = dr["JobComments"].ToString(),
            //            JobDate = Convert.ToDateTime(dr["JobDate"]),
            //            JobModels = Convert.ToInt32(dr["JobModelse"])
            //        }).ToList();

            //    var selectListItems = jobList
            //        .Select(x => new SelectListItem() {Value = x.ToString(), Text = x.ToString()}).ToList();

            Regex csvSplit = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");

            var jobData = File.ReadAllLines(@"..\jobs.csv").Skip(1)
                .Select(x => csvSplit.Split(x))
                .Select(x => new Job()
                {
                    JobId = int.Parse(x[0]),
                    JobCustomer = x[1],
                    JobDate = Convert.ToDateTime(x[2]),
                    JobDuration = int.Parse(x[3]),
                    JobAddress = x[4],
                    JobModels = int.Parse(x[5]),
                    JobComments = x[6]
                });

            foreach (Job j in jobData)
            {
                
                JobList.Add(j.ToString());
            }
            
            
        }
    }
}
