﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

var i = 0;
var txt = 'Log in to see your jobs!'; /* The text */
var speed = 50; /* The speed/duration of the effect in milliseconds */

function typeWriter() {
    if (i < txt.length) {
        document.getElementById("animation").innerHTML += txt.charAt(i);
        i++;
        setTimeout(typeWriter, speed);
    }
}