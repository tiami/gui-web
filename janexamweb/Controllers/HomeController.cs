﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using janexamweb.Data;
using Microsoft.AspNetCore.Mvc;
using janexamweb.Models;
using Microsoft.AspNetCore.Authorization;

namespace janexamweb.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [Authorize]
        public IActionResult Job()
        {
            var JobList = new List<Job>();
            Regex csvSplit = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");

            var jobData = System.IO.File.ReadAllLines(@"..\jobs.csv").Skip(1)
                .Select(x => csvSplit.Split(x))
                .Select(x => new Job()
                {
                    JobId = int.Parse(x[0]),
                    JobCustomer = x[1],
                    JobDate = Convert.ToDateTime(x[2]),
                    JobDuration = int.Parse(x[3]),
                    JobAddress = x[4],
                    JobModels = int.Parse(x[5]),
                    JobComments = x[6]
                });

            foreach (Job j in jobData)
            {

                JobList.Add(j);
            }
            ViewData["JobList"] = JobList;

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
